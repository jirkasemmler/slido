<?php

namespace Tests\Unit;

use App\Restaurant\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{

    public function testParseValidStructure1()
    {
        $in = __DIR__ . '/fixtures/in1Valid.csv';
        $parser = new Parser();

        $out = $parser->parseData($in);

        $this->assertEquals([
            [
                'name' => 'Esther',
                'restaurant_id' => 'esthers',
                'price' => 3,
                'rating' => 3,
                'location' => '22 Teutonic Ave.',
                'description' => 'German home-cooked ...',
                'cuisine' => 'german',
                'days' => [
                    ['day' => 'mo', 'start' => '11:30:00', 'end' => '22:30:00'],
                    ['day' => 'tu', 'start' => '11:30:00', 'end' => '22:30:00'],
                    ['day' => 'we', 'start' => '11:30:00', 'end' => '22:30:00'],
                    ['day' => 'th', 'start' => '11:30:00', 'end' => '22:30:00'],
                    ['day' => 'fr', 'start' => '11:30:00', 'end' => '22:30:00'],
                    ['day' => 'sa', 'start' => '11:30:00', 'end' => '22:30:00']
                ]
            ],

            [
                'name' => 'Robatayaki Hachi',
                'restaurant_id' => 'robatayaki',
                'price' => 4,
                'rating' => 5,
                'location' => '8 Hawthorne Ln.',
                'description' => 'Japanese food ...',
                'cuisine' => 'japanese',
                'days' => [
                    ['day' => 'mo', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'tu', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'we', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'th', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'fr', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'sa', 'start' => '17:30:00', 'end' => '23:30:00'],
                    ['day' => 'su', 'start' => '17:30:00', 'end' => '23:30:00']
                ]
            ],
        ], $out);
    }

    public function testParseValidStructure2()
    {
        $in = __DIR__ . '/fixtures/in2Valid.csv';
        $parser = new Parser();

        $out = $parser->parseData($in);

        $this->assertEquals([

                [
                    'name' => 'Kushi Tsuru',
                    'restaurant_id' => 'KushiTsuru',
                    'price' => null,
                    'rating' => null,
                    'location' => null,
                    'description' => null,
                    'cuisine' => null,
                    'days' => [
                        ['day' => 'mo', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'tu', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'we', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'th', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'fr', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'sa', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'su', 'start' => '11:30:00', 'end' => '21:00:00']
                    ]
                ],
                [
                    'name' => 'Osakaya Restaurant',
                    'restaurant_id' => 'OsakayaRestaurant',
                    'price' => null,
                    'rating' => null,
                    'location' => null,
                    'description' => null,
                    'cuisine' => null,
                    'days' => [
                        ['day' => 'mo', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'tu', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'we', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'th', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'su', 'start' => '11:30:00', 'end' => '21:00:00'],
                        ['day' => 'fr', 'start' => '11:30:00', 'end' => '21:30:00'],
                        ['day' => 'sa', 'start' => '11:30:00', 'end' => '21:30:00'],
                    ]
                ]

        ], $out);
    }

    public function testOneDayMoreTimes()
    {
        $in = __DIR__ . '/fixtures/in2oneDayMoreTimes.csv';
        $parser = new Parser();

        $out = $parser->parseData($in);

        $this->assertEquals([

            [
                'name' => 'Osakaya Restaurant',
                'restaurant_id' => 'OsakayaRestaurant',
                'price' => null,
                'rating' => null,
                'location' => null,
                'description' => null,
                'cuisine' => null,
                'days' => [
                    ['day' => 'su', 'start' => '9:00:00', 'end' => '11:00:00'],
                    ['day' => 'su', 'start' => '13:00:00', 'end' => '23:00:00']
                ]
            ],


        ], $out);
    }

    public function testWrongFile()
    {
        $in = __DIR__ . '/fixtures/xxx.csv';
        $parser = new Parser();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("file {$in} is not accessible");
        $parser->parseData($in);
    }
}
