@php
    use App\OpeningHour;
        /**
        * @var OpeningHour $oh
        */
@endphp

<table>
    @foreach($ohs as $oh)
        <tr {{ $oh->isNow() ? 'class=active' : "" }}>
            <td>{{$oh->day}}</td>
            <td>{{$oh->getStartFormat()}}</td>
            <td>{{$oh->getEndFormat()}}</td>
        </tr>
    @endforeach
</table>
