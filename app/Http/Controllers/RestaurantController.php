<?php


namespace App\Http\Controllers;


use App\Cuisine;
use App\Restaurant;
use Assurrussa\GridView\GridView;
use Illuminate\Foundation\Application;

class RestaurantController extends Controller
{
    protected $restaurant;

    protected const OPEN_FILTER = 'byOpened';
    protected const RATING_FILTER = 'byRating';
    protected const PRICE_FILTER = 'byPrice';
    protected const CUISINE_FILTER = 'byCuisineId';
    protected const GLOBAL_SEARCH_FILTER = 'search';

    public function __construct(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    public function index()
    {
        $title = __('Restaurants');
        $data = $this->getGrid()->getSimple();
        if (request()->ajax() || request()->wantsJson()) {
            return $data->toHtml();
        }

        return view('restaurant.index', compact('title', 'data'));
    }

    /**
     * builds the grid and manages the sorting/searching
     * @return GridView|Application|mixed
     */
    public function getGrid()
    {
        $listCuisine = Cuisine::all('id', 'name as label')->toArray();
        $listCuisineSelected = [];
        if ($cuisineId = (int)\request()->get(self::CUISINE_FILTER)) {
            /** @var Cuisine $model */
            $model = Cuisine::find($cuisineId);
            $listCuisineSelected = [
                'label' => $model->name,
                'id' => $model->id,
            ];
        }

        $openedSelected = (string)\request()->get(self::OPEN_FILTER);
        $ratingSelected = (string)\request()->get(self::RATING_FILTER);
        $priceSelected = (string)\request()->get(self::PRICE_FILTER);
        $searchInput = (string)\request()->get(self::GLOBAL_SEARCH_FILTER);

        /** @var GridView $gridView */
        $query = $this->restaurant->newQuery()
            ->with(['cuisine', 'openingHours']);

        if ($searchInput) {
            $likeSearchInput = "%{$searchInput}%";
            $query->where('name', 'LIKE', $likeSearchInput)->orWhere('location', 'LIKE', $likeSearchInput);
        }
        $gridView = app(GridView::NAME);

        $gridView->setQuery($query)
            ->setSearchInput(true)
            ->setSortName('name');

        $gridView->column()->setClassForString(function ($data) {
            /** @var \App\Restaurant $data */
            return $data->isOpened() ? 'text-success' : '';
        });

        // columns
        $gridView->column('name', __('Name'))
            ->setSort(true)
            ->setFilterString('byName');

        $gridView->column('price', __('Price'))
            ->setSort(true)
            ->setScreening(true)
            ->setFilterString(self::PRICE_FILTER)
            ->setHandler(function ($model) {
                /** @var \App\Restaurant $model */
                return str_repeat('<span class="material-icons">attach_money</span>', $model->price);
            })
            ->setFilterSelect(self::PRICE_FILTER, [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], $priceSelected);;

        $gridView->column('rating', __('Rating'))
            ->setScreening(true)
            ->setSort(true)
            ->setFilterString(self::RATING_FILTER)
            ->setHandler(function ($model) {
                return view('restaurant.stars', ['rating' => $model->rating])->render();
            })
            ->setFilterSelect(self::RATING_FILTER, [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], $ratingSelected);;

        $gridView->column('location', __('Location'));
        $gridView->column('opened', __('Opened'))
            ->setScreening(true)
            ->setHandler(function ($model) {
                /** @var \App\Restaurant $model */
                return $model->isOpened() ? __('Yes') : __('No');
            })
            ->setFilterSelect('byOpened', ['true' => __('Yes'), 'false' => __('No')], $openedSelected);

        $gridView->column('hours', 'hours')
            ->setScreening(true)
            ->setHandler(function ($model) {
                /** @var \App\Restaurant $model */
                return $view = view('restaurant.opening', ['ohs' => $model->openingHours()->get()])->render();
            });

        $gridView->column('cuisine.name', 'Cuisine')->setSort(false)->setScreening(false)
            ->setFilterSelectNotAjax('byCuisineId', $listCuisine, $listCuisineSelected);

        return $gridView;
    }

}
