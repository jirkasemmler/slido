<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Restaurant
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $restaurant_id
 * @property int $price
 * @property int $rating
 * @property string $location
 * @property string $description
 */
class Restaurant extends Model
{
    public static $days = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];

    public function cuisine()
    {
        return $this->belongsTo(Cuisine::class, 'id_cuisine');
    }

    public function openingHours()
    {
        return $this->hasMany(OpeningHour::class, 'id_restaurant');
    }

    /**
     * @param Builder $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByCuisineId(Builder $query, $string)
    {
        return $query->whereHas('cuisine', function (Builder $query) use ($string) {
            return $query->where('id', '=', $string);
        });
    }

    /**
     * @param Builder $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByName(Builder $query, $string)
    {
        return $query->where('name', 'LIKE', "%{$string}%");
    }

    /**
     * @param Builder $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByRating(Builder $query, $string)
    {
        return $query->where('rating', $string);
    }

    /**
     * @param Builder $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByPrice(Builder $query, $string)
    {
        return $query->where('price', $string);
    }

    /**
     * @param Builder $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByOpened(Builder $query, $string)
    {
        if ($string == 'true') {
            return $query->whereHas('openingHours', function (Builder $query) use ($string) {
                return $this->buildOpenHourQuery($query);
            });

        } else {
            return $query->whereDoesntHave('openingHours', function (Builder $query) use ($string) {
                return $this->buildOpenHourQuery($query);
            });
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isOpened()
    {
        $isOpen = $this->buildOpenHourQuery($this->openingHours())->exists();
        return $isOpen;
    }

    /**
     * @param $q
     * @return Builder|HasMany
     * @throws \Exception
     */
    protected function buildOpenHourQuery( $q)
    {
        $now = new Datetime();
        $time = $now->format("H:i:s");

        return $q->where('day', self::$days[$now->format('w')])
            ->where('start', '<=', $time)
            ->where(function ($q) use ($time) {
                $q->where('end', '>=', $time)
                    ->orWhere('end', OpeningHour::END_TIME);
            });
    }
}
