<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed day
 * @property mixed start
 * @property mixed end
 */
class OpeningHour extends Model
{
    public const END_TIME = '00:00:00';
    //
    private $startFormat;
    private $endFormat;

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'id_restaurant');
    }

    public function getStartFormat()
    {
        return substr_replace($this->start, '', -3);
    }

    public function getEndFormat()
    {
        return substr_replace($this->end, '', -3);
    }

    public function isNow()
    {
        $now = new Datetime();
        $time = $now->format("H:i");

        return Restaurant::$days[$now->format('w')] == $this->day && $this->start <= $time && ($this->end >= $time || $this->end == self::END_TIME);
    }
}
