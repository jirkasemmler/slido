<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 */
class Cuisine extends Model
{
    public function restaurants()
    {
        return $this->hasMany(Restaurant::class);
    }

}
