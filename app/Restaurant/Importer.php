<?php


namespace App\Restaurant;


use App\Cuisine;
use App\OpeningHour;
use App\Restaurant;

class Importer
{
    protected $cuisines;
    protected $restaurants;

    public function __construct()
    {
        $this->cuisines = Cuisine::all()->keyBy('name');
        $this->restaurants = Restaurant::all()->keyBy('restaurant_id');
    }

    /**
     * imports the parsed data to DB
     * @param $data
     * @return bool
     */
    public function import($data)
    {
        foreach ($data as $item) {
            /**
             * @var array $line
             */

            if (!($restaurantObj = $this->restaurants->get($item['restaurant_id']))) {
                $restaurantObj = new Restaurant();
            }

            $restaurantObj->name = $item['name'];
            $restaurantObj->restaurant_id = $item['restaurant_id'];
            $restaurantObj->price = $item['price'];
            $restaurantObj->rating = $item['rating'];
            $restaurantObj->location = $item['location'];
            $restaurantObj->description = $item['description'];

            $cuisineName = $item['cuisine'];

            // add cuisine
            if ($cuisineName) {

                if (!($cuisine = $this->cuisines->get($cuisineName))) {
                    $cuisine = new Cuisine();
                    $cuisine->name = $cuisineName;

                    $cuisine->save();
                    $this->cuisines[$cuisineName] = $cuisine;
                }

                $restaurantObj->cuisine()->associate($cuisine);
                $restaurantObj->save();
            }

            $restaurantObj->save();

            // store opening hours
            $usedIds = [];
            foreach ($item['days'] as $day) {
                $oh = $restaurantObj->openingHours()->where('day', $day['day'])->where('start', $day['start'])->where('end', $day['end'])->first();

                // if OH exists, just keep it
                if (!$oh) {
                    $oh = new OpeningHour();
                    $oh->day = $day['day'];
                    $oh->start = $day['start'];
                    $oh->end = $day['end'];
                    $oh->restaurant()->associate($restaurantObj);

                    $oh->save();
                }

                $usedIds[] = $oh->id;
            }

            // delete the ones which belongs to restaurant, but dont exist anymore in input data
            $restaurantObj->openingHours()->whereNotIn('id', $usedIds)->delete();

        }

        return true;
    }
}
