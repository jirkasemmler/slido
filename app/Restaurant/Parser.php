<?php


namespace App\Restaurant;

class Parser
{
    protected static $days = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];

    /*
     * data can come in LONG or SHORT form -> long/Short parser
     *
     * LONG - with HEAD
     * Restaurant name, Restaurant ID, Cuisine, Opens, Closes, Days Open, Price, Rating, Location, Description
     *
     * SHORT
     * "Kushi Tsuru", "Mon-Sun 11:30 am - 9 pm"
     */

    /**
     * @param string $path
     * @return array
     * @throws \Exception
     */
    public function parseData(string $path)
    {
        if (is_readable($path) && ($handle = fopen($path, "r")) !== false) {
            $out = [];

            $firstLine = fgetcsv($handle, 0, ",");

            // name of function/method to use. LongParser by default
            $methodToUse = 'longParser';

            if (count($firstLine) == 2) {
                $methodToUse = 'shortParser';

                // rewinding because short format doesn't have any HEAD
                rewind($handle);
            }

            while (($line = fgetcsv($handle, 0, ",")) !== false) {
                $out[] = $this->$methodToUse($line);
            }

            fclose($handle);
            return $out;
        } else {
            throw new \Exception("file {$path} is not accessible");
        }
    }

    /**
     * @param $line
     * @return array
     * returns structured data of form
     * [
     *   [
     *      name => aaa
     * restaurant_id => aaa
     * price => 3
     * rating => 1
     * location => bbb
     * description => ccc
     * cuisine => czech
     *      days => [
     *                 ['day' => 'mo', 'start' => 11:00, 'end' => 12:00],
     *                 ...
     *              ]
     *   ],
     * ...
     * ]
     */
    protected function longParser($line)
    {
        // one line
        //   0                 1              2      3          4        5                  6         7     8                   9
        // Restaurant name, Restaurant ID, Cuisine, Opens,     Closes,  Days Open,          Price, Rating, Location,         , Description
        // Esther's German, esthers      , german , 11:30:00,  22:30:00,"Mo,Tu,We,Th,Fr,Sa", 3   ,   3   ,  22 Teutonic Ave. ,"Ger..."

        // setting available data
        $data = [
            'name' => trim($line[0] ?? 'unknown'),
            'restaurant_id' => trim($line[1] ?? ''),
            'price' => (int)$line[6] ?? null,
            'rating' => (int)$line[7] ?? null,
            'location' => trim($line[8] ?? null),
            'description' => trim($line[9] ?? null),
            'cuisine' => trim($line[2] ?? null),
            'days' => []
        ];

        // matching time to each day
        $days = explode(',', trim($line[5]));
        $days = array_map('strtolower', $days);
        $daysToStore = [];
        $start = $line[3] ?? 'n/a';
        $end = $line[4] ?? 'n/a';

        // generating structure for each day
        foreach ($days as $day) {
            $daysToStore[] = ['day' => $day, 'start' => $start, 'end' => $end];
        }

        $data['days'] = $daysToStore;
        return $data;
    }

    /**
     * @param $line
     * @return array
     * returns structured data of form
     * [
     *   [
     *      name => aaa
     *      days => [
     *                 ['day' => 'mo', 'start' => 11:00, 'end' => 12:00],
     *                 ...
     *              ]
     *   ],
     * ...
     * ]
     */
    protected function shortParser($line)
    {
        // one line
        //  0               1
        // "Kushi Tsuru" ,"Mon-Wed 5 pm - 12:30 am  / Thu-Fri 5 pm - 1:30 am  / Sat 3 pm - 1:30 am  / Sun 3 pm - 11:30 pm"

        $data = [
            'name' => trim($line[0]),
            'restaurant_id' => preg_replace('/[^A-Za-z0-9\-]/', '', $line[0]),
            'price' => null,
            'rating' => null,
            'location' => null,
            'description' => null,
            'cuisine' => null,
            'days' => []
        ];

        // multiple openingHours can come serialized and joined by / -> parse one by one
        $openingHours = explode('/', trim($line[1]));
        $openingHours = array_map('trim', $openingHours);

        // for each Mon-Wed 5 pm - 12:30 am entry
        foreach ($openingHours as $time) {
            $time = strtolower($time);

            // matching time and date separately
            //                   days   start                    end
            preg_match('/(.*) (\d\d?:?\d?\d?).(..) - (\d\d?:?\d?\d?) (..)/', $time, $out);

            // one openingHour entry can represent more days in form of day or range : Thu OR Mon-Wed OR Mon-Wed, Thu
            $days = explode(',', $out[1]);
            $days = array_map('trim', $days);

            $allDays = [];
            foreach ($days as $dayBlock) {
                preg_match('/(\w{3})\-(\w{3})/', $dayBlock, $matchInterval);

                // it was a range Mon-Wed
                if ($matchInterval) {
                    $start = $matchInterval[1];
                    $end = $matchInterval[2];
                    $allDays = array_merge($allDays, $this->generateDays($start, $end));
                } else {
                    // single day
                    $allDays[] = substr($dayBlock,0,2);
                }
            }

            $allDays = array_flip($allDays);

            $startTime = $this->normalizeTime($out[2], $out[3]);
            $endTime = $this->normalizeTime($out[4], $out[5]);

            foreach ($allDays as $key => $val) {
                $data['days'][] = ['day' => $key, 'start' => $startTime, 'end' => $endTime];
            }
        }

        return $data;
    }

    /**
     * transforms mo-we to [mo, tu, we]
     * @param $start
     * @param $end
     * @return array
     */
    protected function generateDays($start, $end)
    {
        $startIndex = array_search(substr($start, 0, 2), self::$days);
        $endIndex = array_search(substr($end, 0, 2), self::$days);

        return array_slice(self::$days, $startIndex, ($endIndex - $startIndex) + 1);
    }

    /**
     * transforms time from format HH(:MM)? to format HH:MM and transforms to 24h format in case of PM/AM
     * @param $time
     * @param $modifier
     * @return string
     */
    protected function normalizeTime($time, $modifier)
    {
        if (is_int($time) || strlen($time) <= 2) {
            $time .= ":00";
        }

        if ($modifier == 'pm') {
            $separated = explode(':', $time);
            $separated[0] = ((int)$separated[0]) + 12;
            $time = implode(":", $separated);
        }

        return ($time . ":00");
    }
}
