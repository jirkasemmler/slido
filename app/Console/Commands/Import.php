<?php

namespace App\Console\Commands;

use App\Restaurant\Importer;
use App\Restaurant\Parser;
use Illuminate\Console\Command;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports restaurants to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');

        $parser = new Parser();
        $parsedData = $parser->parseData($path);
        if($parsedData){
            $importer = new Importer();
            $importer->import($parsedData);
        }

    }


}
