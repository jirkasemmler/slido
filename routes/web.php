<?php
/** @var \Illuminate\Routing\Router $router */

$router->group(['prefix' => '', 'as' => 'post.',], function ($router) {
    /** @var \Illuminate\Routing\Router $router */
    $router->get('', 'RestaurantController@index')->name('index');
});
