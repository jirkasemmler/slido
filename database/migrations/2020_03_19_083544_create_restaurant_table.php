<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('restaurant_id', 100)->nullable();
            $table->foreignId('id_cuisine')->nullable();
            $table->integer('price')->nullable();
            $table->integer('rating')->nullable();
            $table->string('location', 255)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('id_cuisine')->references('id')->on('cuisines');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant');
    }
}
