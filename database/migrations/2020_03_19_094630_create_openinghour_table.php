<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeninghourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_hours', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_restaurant');
            // enums for very specific types only. The ones, which won't change
            $table->enum('day', ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']);
            $table->time('start');
            $table->time('end');
            $table->timestamps();

            $table->foreign('id_restaurant')->references('id')->on('restaurants');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opening_hours');
    }
}
